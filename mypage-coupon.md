# Shopify レクチャー資料

## 基本

### テストサイト

URL: https://maruku-develop.myshopify.com/
Password: eladri

管理画面: https://maruku-develop.myshopify.com/admin/
メールアドレス: develop@maruku.biz

## 定期購入の設定

例: [定期購入対象商品](https://maruku-develop.myshopify.com/products/%E5%AE%9A%E6%9C%9F%E8%B3%BC%E5%85%A5%E3%81%99%E3%82%8B%E5%95%86%E5%93%81-1)
1. アプリ管理
1. Subscription Recurring order を選択
1. 画面の説明
	1. Dashboard - 定期購入の状況を確認できます
	1. Subscription - 定期購入を設定します
	1. Customer - 定期購入利用中のお客様を確認できます
	1. Order - 定期購入の注文を確認できます
	1. Email - 定期購入に関するメールを編集できます
	1. Subscription Layout - 定期購入に関するレイアウトを編集できます
	1. Language - 定期購入に関する文言を編集できます
	1. Customer Portal - お客様の定期購入管理画面にかんする設定ができます
1. 定期購入を新規設定する
	1. Subscription を選択
	2. Add Subscription を選択
	3. 定期購入の名前を入力します Plan Label / Plan Name に定期購入名を入力して Next をおします
	4. Choose Product を押して定期購入対象の商品を選択します
	5. Add Frequency を押して、定期購入の詳細を設定します
	6. Frequency Name - 定期購入期間の名前
	7. Order Frequency - 定期購入期間の数字
	8. Order Frequency Type - 定期購入期間の単位
	9. Bill Every ... - 決済タイミング
	10. Discount Type - 定期購入割引のタイプ（なし、割引、数量、任意の価格）
	11. Discount Value - 定期購入割引数量
	12. Save Frequency を押して定期購入設定を保存
	13. Save Contract を押して設定を完了する
 
